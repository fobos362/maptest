package com.dataqlick.shpp.mapsapp;


import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import java.util.ArrayList;

public class UniversalSelectionAdapter extends RecyclerView.Adapter {

    public static class mViewHolder extends RecyclerView.ViewHolder {
        TextView mTextView;
        CheckBox mCheckBox;

        public mViewHolder(View itemView) {
            super(itemView);
            mTextView = (TextView) itemView.findViewById(R.id.uni_selection_text);
            mCheckBox = (CheckBox) itemView.findViewById(R.id.uni_selection_check_box);
        }
    }

    ArrayList<LovelyPlace> data;

    public UniversalSelectionAdapter(ArrayList<LovelyPlace> incomingData) {
        data = incomingData;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.love_item, parent, false);
        return new mViewHolder(v);
    }



    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        mViewHolder mholder = (mViewHolder) holder;
        mholder.mTextView.setText(data.get(position).mDescription);
    }

    @Override
    public int getItemCount() {
        return data.size();
    }
}
