package com.dataqlick.shpp.mapsapp;

import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

/**
 * Created by Shpp on 27.05.2016.
 */
public class InfoFragment extends DialogFragment implements View.OnClickListener {

    public static final String TAG = "rvux:plfildlg";
    protected View mView;
    protected ArrayList<LovelyPlace> allData;
    protected RecyclerView mRecyclerView;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.info_fragment, null);
        allData = ((MapsActivity)getActivity()).getPlaces();


        mRecyclerView = (RecyclerView) mView.findViewById(R.id.places_rv);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        mRecyclerView.setAdapter(new UniversalSelectionAdapter(allData));

        return mView;
    }

    public void onClick(View v) {

    }


}
