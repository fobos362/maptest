package com.dataqlick.shpp.mapsapp;

import com.google.android.gms.location.places.Place;


public class LovelyPlace {
    Place mPlace;
    String mDescription;

    LovelyPlace(Place p, String d){
        mPlace = p;
        mDescription = d;
    }
}
