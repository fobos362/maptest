package com.dataqlick.shpp.mapsapp;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.PlaceLikelihood;
import com.google.android.gms.location.places.PlaceLikelihoodBuffer;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, View.OnClickListener {

    public final static String TAG = "main_act";
    protected GoogleMap mMap;
    protected GoogleApiClient mGoogleApiClient;
    protected FloatingActionButton mLovePlaceButton;
    public ArrayList<LovelyPlace> selectedPlaces = new ArrayList<>();
    public ArrayList<Place> mPlaceList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        mLovePlaceButton = (FloatingActionButton) findViewById(R.id.love_place);
        mLovePlaceButton.setOnClickListener(this);

        checkConnection();
        checkLocationPermission();

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        mGoogleApiClient = new GoogleApiClient
                .Builder(this)
                .addApi(LocationServices.API)
                .addApi(Places.GEO_DATA_API)
                .addApi(Places.PLACE_DETECTION_API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();
        mGoogleApiClient.connect();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {

                return false;
            }
        });

    }

    protected void checkConnection() {
        if (!Utils.isNetworkAvailable(this))
            Toast.makeText(this, R.string.check_connection, Toast.LENGTH_SHORT).show();
    }

    protected void checkLocationPermission() {
        if (!Utils.isNetworkAvailable(this))
            Toast.makeText(this, R.string.check_policy, Toast.LENGTH_SHORT).show();
    }


    @Override
    public void onConnected(@Nullable Bundle bundle) {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            return;
        }
        Location mLastLocation = LocationServices.FusedLocationApi.getLastLocation(
                mGoogleApiClient);
        if (mLastLocation != null) {
            LatLng iam = new LatLng(mLastLocation.getLatitude(), mLastLocation.getLongitude());
            Utils.setMarker(mMap, new MarkerOptions()
                    .position(iam)
                    .title("I am here")
                    .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN)));
            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(iam, 16.0f));


            PendingResult<PlaceLikelihoodBuffer> result = Places.PlaceDetectionApi
                    .getCurrentPlace(mGoogleApiClient, null);
            result.setResultCallback(new ResultCallback<PlaceLikelihoodBuffer>() {
                @Override
                public void onResult(PlaceLikelihoodBuffer likelyPlaces) {
                    Log.d("TAG", "res");
                    for (PlaceLikelihood placeLikelihood : likelyPlaces) {
                        mPlaceList.add(placeLikelihood.getPlace());
                        selectedPlaces.add(new LovelyPlace(placeLikelihood.getPlace(), (String) placeLikelihood.getPlace().getAttributions()));
                        Utils.setMarker(mMap, new MarkerOptions()
                                .position(placeLikelihood.getPlace().getLatLng())
                                .title((String) placeLikelihood.getPlace().getName())
                                .icon(BitmapDescriptorFactory.defaultMarker(placeLikelihood.getLikelihood()> 0 ?
                                                                        BitmapDescriptorFactory.HUE_GREEN:BitmapDescriptorFactory.HUE_RED)));

                    }
                    likelyPlaces.release();
                }
            });
        }
    }

    @Override
    public void onConnectionSuspended(int i) {
        Toast.makeText(this, R.string.check_connection, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Toast.makeText(this, R.string.check_connection, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.love_place:
                PlacesListFragment filterDialogFragment = new PlacesListFragment();
                filterDialogFragment.setStyle(DialogFragment.STYLE_NO_TITLE, R.style.FilterDialog);
                filterDialogFragment.show(getSupportFragmentManager(), filterDialogFragment.getTag());
                break;
        }


    }

    public ArrayList<LovelyPlace> getPlaces() {
        return selectedPlaces;
    }


}
